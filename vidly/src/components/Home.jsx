import React from 'react';

class Home extends React.Component{
    render() {
        return(
            <div>
                <h1>Welcome to reminder.me</h1>
            </div>
        );
    }
}

export default Home;